import Button from '@atlaskit/button';
import Textfield from '@atlaskit/textfield';
import { useState } from 'react';
import TodoList from "./components/TodoList";

function App() {
  const [textInput, setTextInput] = useState("");
  const onInputChange = (event) => {
    setTextInput(event.target.value);
  }

  return (
    <div>
      <h1>List todos</h1>
      <Textfield 
        onChange={onInputChange} 
        elemAfterInput={<Button appearance="primary" isDisabled={!textInput}>Add todo</Button>}
      />
      <TodoList />
    </div>
  );
}

export default App;
